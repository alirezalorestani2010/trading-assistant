package com.example.tradingassistant.core.data

import androidx.annotation.Keep

sealed class Result<out T> {

    object Loading : Result<Nothing>()

    @Keep
    data class Success<out T>(val data: T) : Result<T>()

    @Keep
    data class Error<out T>(val message: String, val data: T? = null) : Result<T>()

    override fun toString(): String {
        return when (this) {
            is Loading -> "Loading"
            is Success<*> -> "Success[data=$data]"
            is Error<*> -> "Error[exception=$message, data=$data]"
        }
    }
}