package com.example.tradingassistant.core.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.tradingassistant.features.filter.data.FilterDao
import com.example.tradingassistant.features.filter.data.entity.FilterEntity
import com.example.tradingassistant.features.filter.data.entity.FilterGroupEntity
import com.example.tradingassistant.features.filter.data.entity.FilterParamEntity


@Database(
    entities = [FilterEntity::class, FilterGroupEntity::class, FilterParamEntity::class],
    version = 1
    )
abstract class TradingAssistantDataBase : RoomDatabase() {

    abstract fun filterDao(): FilterDao

    companion object {

        @Volatile
        private var INSTANCE: TradingAssistantDataBase? = null

        fun createDatabase(context: Context): TradingAssistantDataBase {
            // if the INSTANCE is not null, then return it,
            // if it is, then create the database
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    TradingAssistantDataBase::class.java,
                    "trading-assistant.db"
                ).build()
                INSTANCE = instance
                // return instance
                instance
            }
        }

        fun getDatabase(): TradingAssistantDataBase? = INSTANCE

    }


}