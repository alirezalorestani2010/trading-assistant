package com.example.tradingassistant.core.data

import com.google.gson.annotations.SerializedName

open class BaseResponse(
    @SerializedName("status_code")
    val statusCode: Int,
    @SerializedName("status_text")
    val statusText: String
)