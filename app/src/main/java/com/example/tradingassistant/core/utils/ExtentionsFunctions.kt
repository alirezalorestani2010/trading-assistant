package com.example.tradingassistant.core.utils

import android.widget.Toast
import androidx.fragment.app.Fragment

fun Fragment.onBackPressInBaseFragments(mBackPressed: Long): Long {
    var lastPressed = mBackPressed

    val TIME_INTERVAL = 1000
    if (lastPressed + TIME_INTERVAL > System.currentTimeMillis()) {
        requireActivity().finish()
        return 0
    } else {
        Toast.makeText(requireContext(), "Press again to exit", Toast.LENGTH_SHORT)
            .show()
    }
    lastPressed = System.currentTimeMillis()
    return lastPressed
}


