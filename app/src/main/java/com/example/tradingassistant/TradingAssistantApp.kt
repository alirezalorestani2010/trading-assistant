package com.example.tradingassistant

import android.app.Application
import com.example.tradingassistant.core.data.TradingAssistantDataBase

class TradingAssistantApp : Application() {

    override fun onCreate() {
        super.onCreate()
        val database = TradingAssistantDataBase.createDatabase(applicationContext)
    }
}