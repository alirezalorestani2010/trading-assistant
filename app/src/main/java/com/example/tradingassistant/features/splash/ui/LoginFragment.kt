package com.example.tradingassistant.features.splash.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.tradingassistant.R
import com.example.tradingassistant.databinding.FragmentLoginBinding
import com.google.android.material.snackbar.Snackbar

class LoginFragment : Fragment() {
    private lateinit var binding: FragmentLoginBinding
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_login, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.apply {
            btnLoginFragment.setOnClickListener {
                if (informationIsValid())
                    findNavController().navigate(LoginFragmentDirections.actionLoginFragmentToFiltersNavGraph())
            }
        }
    }

    private fun informationIsValid(): Boolean {

        if (binding.etUsernameLoginFragment.text.isEmpty()) {
            binding.etUsernameLoginFragment.error = "Enter Username"
            return false
        }
        if (binding.etPasswordLoginFragment.text.isEmpty()) {
            binding.etPasswordLoginFragment.error = "Enter Password"
            return false
        }
        if (binding.etUsernameLoginFragment.text.toString() == "admin" && binding.etPasswordLoginFragment.text.toString() == "admin")
            return true
        else
            Snackbar.make(binding.root, "Information is not correct", Snackbar.LENGTH_SHORT).show()
        return false

    }
}