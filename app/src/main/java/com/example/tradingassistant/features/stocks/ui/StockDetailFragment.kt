package com.example.tradingassistant.features.stocks.ui

import android.graphics.Color
import android.graphics.Paint
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.example.tradingassistant.R
import com.example.tradingassistant.core.data.EventObserver
import com.example.tradingassistant.core.data.Result
import com.example.tradingassistant.databinding.FragmentStockDetailBinding
import com.github.mikephil.charting.components.Legend
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.components.YAxis
import com.github.mikephil.charting.data.*
import com.github.mikephil.charting.formatter.LargeValueFormatter
import java.util.*


class StockDetailFragment : Fragment() {
    private val sharedViewModel: StockViewModel by activityViewModels()
    private lateinit var binding: FragmentStockDetailBinding
    private val TAG = this.javaClass.simpleName


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_stock_detail, container, false)


        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)



        sharedViewModel.vp.observe(viewLifecycleOwner, EventObserver {
            when (it) {
                is Result.Error -> {
                    binding.clLoadingStateStocksDetailFragment.visibility = View.GONE

                }
                is Result.Loading -> {
                    binding.clLoadingStateStocksDetailFragment.visibility = View.VISIBLE

                }
                is Result.Success -> {
                    binding.clLoadingStateStocksDetailFragment.visibility = View.GONE
                    findNavController().navigate(R.id.action_stockDetailFragment_to_vpFragment)
                }
            }

        })
        when (findNavController().currentDestination?.id) {
            R.id.stockDetailFragmentInFilterNavGraph -> {
                Log.d(
                    "ALIREZA",
                    "filter"
                )
                binding.apply {
                    etVpPeriodStockDetailFragment.visibility = View.GONE
                    btnGetVpStockDetailFragment.visibility = View.GONE
                }
            }
            R.id.stockDetailFragmentInRl -> {
                Log.d(
                    "ALIREZA",
                    "rl"
                )
                binding.apply {
                    etVpPeriodStockDetailFragment.visibility = View.GONE
                    btnGetVpStockDetailFragment.visibility = View.GONE
                }

            }
            R.id.stockDetailFragment -> {
                Log.d(
                    "ALIREZA",
                    "stocks"
                )
                binding.apply {
                    btnGetVpStockDetailFragment.setOnClickListener {
                        when {
                            etVpPeriodStockDetailFragment.text.isNullOrEmpty() -> {
                                etVpPeriodStockDetailFragment.error = "بازه ی زمانی را مشخص کنید"
                            }
                            etVpPeriodStockDetailFragment.text.toString().toInt() < 10 -> {
                                etVpPeriodStockDetailFragment.error =
                                    "بازه ی زمانی باید از 10 بزرگتر باشد"
                            }
                            else -> {
                                sharedViewModel.getVp(
                                    stockDetail?.id ?: "",
                                    etVpPeriodStockDetailFragment.text.toString().toInt()
                                )
                            }
                        }
                    }
                }
            }
        }

        binding.apply {

            customizeChartView()
            if (sharedViewModel.stockDetail.value?.peekContent() is Result.Success) {
                val stock =
                    (sharedViewModel.stockDetail.value?.peekContent() as Result.Success).data
                binding.stockDetail = stock.detail


                val volumeChartValue1 = ArrayList<BarEntry>()
                volumeChartValue1.add(BarEntry(1f, stock.detail.RL.volumeBuyLegal.toFloat()))
                val volumeChartValue2 = ArrayList<BarEntry>()
                volumeChartValue2.add(BarEntry(2f, stock.detail.RL.volumeSellLegal.toFloat()))
                val volumeChartValue3 = ArrayList<BarEntry>()
                volumeChartValue3.add(BarEntry(3f, stock.detail.RL.volumeBuyReal.toFloat()))
                val volumeChartValue4 = ArrayList<BarEntry>()
                volumeChartValue4.add(BarEntry(4f, stock.detail.RL.volumeSellReal.toFloat()))

                val valueChartValue1 = ArrayList<BarEntry>()
                valueChartValue1.add(BarEntry(1f, stock.detail.RL.valueBuyLegal.toFloat()))
                val valueChartValue2 = ArrayList<BarEntry>()
                valueChartValue2.add(BarEntry(2f, stock.detail.RL.valueSellLegal.toFloat()))
                val valueChartValue3 = ArrayList<BarEntry>()
                valueChartValue3.add(BarEntry(3f, stock.detail.RL.valueBuyReal.toFloat()))
                val valueChartValue4 = ArrayList<BarEntry>()
                valueChartValue4.add(BarEntry(4f, stock.detail.RL.valueSellReal.toFloat()))


                //volume
                // create 4 DataSets
                val volumeSet1 = BarDataSet(volumeChartValue1, "حجم خرید حقوقی")
                volumeSet1.color = Color.rgb(104, 241, 175)
                val volumeSet2 = BarDataSet(volumeChartValue2, "حجم فروش حقوقی")
                volumeSet2.color = Color.rgb(164, 228, 251)
                val volumeSet3 = BarDataSet(volumeChartValue3, "حجم خرید حقیقی")
                volumeSet3.color = Color.rgb(242, 247, 158)
                val volumeSet4 = BarDataSet(volumeChartValue4, "حجم فروش حقیقی")
                volumeSet4.color = Color.rgb(255, 102, 0)


                val valueSet1 = BarDataSet(valueChartValue1, "ارزش خرید حقوقی")
                valueSet1.color = Color.rgb(104, 241, 175)
                val valueSet2 = BarDataSet(valueChartValue2, "ارزش فروش حقوقی")
                valueSet2.color = Color.rgb(164, 228, 251)
                val valueSet3 = BarDataSet(valueChartValue3, "ارزش خرید حقیقی")
                valueSet3.color = Color.rgb(242, 247, 158)
                val valueSet4 = BarDataSet(valueChartValue4, "ارزش فروش حقیقی")
                valueSet4.color = Color.rgb(255, 102, 0)

                val volumeData = BarData(volumeSet1, volumeSet2, volumeSet3, volumeSet4)
                volumeData.setValueTextColor(Color.WHITE)

                val valueData = BarData(valueSet1, valueSet2, valueSet3, valueSet4)
                valueData.setValueTextColor(Color.WHITE)

                bcValueStockDetailFragment.apply {
                    description.isEnabled = false
                    setPinchZoom(false)
                    setDrawGridBackground(false)
                    legend.apply {
                        orientation = Legend.LegendOrientation.HORIZONTAL
                        textColor = Color.WHITE
                        verticalAlignment = Legend.LegendVerticalAlignment.BOTTOM
                        horizontalAlignment = Legend.LegendHorizontalAlignment.CENTER
                        textSize = 8f
                        isWordWrapEnabled = true
                    }
                    setDrawBorders(false)

                }
                bcVolumeStockDetailFragment.apply {
                    description.isEnabled = false
                    setPinchZoom(false)
                    setDrawGridBackground(false)
                    legend.apply {
                        orientation = Legend.LegendOrientation.HORIZONTAL
                        textColor = Color.WHITE
                        verticalAlignment = Legend.LegendVerticalAlignment.BOTTOM
                        horizontalAlignment = Legend.LegendHorizontalAlignment.CENTER
                        textSize = 8f
                        isWordWrapEnabled = true

                    }
                    setDrawBorders(false)
                }
                val yAxisVolumeBar: YAxis = bcVolumeStockDetailFragment.axisLeft
                val rightAxisVolumeBar: YAxis = bcVolumeStockDetailFragment.axisRight
                yAxisVolumeBar.setDrawGridLines(false)
                yAxisVolumeBar.setDrawLabels(false)
                rightAxisVolumeBar.setDrawGridLines(false)
                rightAxisVolumeBar.setDrawLabels(false)
                val xAxisVolumeBar: XAxis = bcVolumeStockDetailFragment.xAxis
                xAxisVolumeBar.setDrawGridLines(false)
                xAxisVolumeBar.setDrawLabels(false)

                val yAxisValueBar: YAxis = bcValueStockDetailFragment.axisLeft
                val rightAxisValueBar: YAxis = bcValueStockDetailFragment.axisRight
                yAxisValueBar.setDrawGridLines(false)
                yAxisValueBar.setDrawLabels(false)
                rightAxisValueBar.setDrawGridLines(false)
                rightAxisValueBar.setDrawLabels(false)
                val xAxisValueBar: XAxis = bcValueStockDetailFragment.xAxis
                xAxisValueBar.setDrawGridLines(false)
                xAxisValueBar.setDrawLabels(false)
//
                volumeData.setValueFormatter(LargeValueFormatter())
                valueData.setValueFormatter(LargeValueFormatter())

                bcVolumeStockDetailFragment.data = volumeData
                bcValueStockDetailFragment.data = valueData

//                val test = arrayListOf<CandleEntry>(
//                    CandleEntry(0F, 225.0F, 219.84F, 224.94F, 221.07F),
//                    CandleEntry(1F, 228.35F, 222.57F, 223.52F, 226.41F),
//                    CandleEntry(2F, 226.84F, 222.52F, 225.75F, 223.84F),
//                    CandleEntry(3F, 222.95F, 217.27F, 222.15F, 217.88F)
//                )


                val candleDataSet = CandleDataSet(
//                    test,
                    stock.toCandleEntry()
                        .takeLast(100),
                    "DataSet 1"
                )

                candleDataSet.color = Color.rgb(80, 80, 80)
                candleDataSet.shadowColor = resources.getColor(R.color.medium_gray)
                candleDataSet.shadowWidth = 0.8f
                candleDataSet.decreasingColor = resources.getColor(R.color.red)
                candleDataSet.decreasingPaintStyle = Paint.Style.FILL
                candleDataSet.increasingColor = resources.getColor(R.color.green)
                candleDataSet.increasingPaintStyle = Paint.Style.FILL
                candleDataSet.neutralColor = Color.LTGRAY
                candleDataSet.setDrawValues(false)

                val candleData = CandleData()
                candleData.addDataSet(candleDataSet)

                cscStockDetail.data = candleData
                cscStockDetail.invalidate()
            }
        }
        sharedViewModel.apply {

        }
    }


    private fun customizeChartView() {
        binding.apply {
            cscStockDetail.isHighlightPerDragEnabled = true

            cscStockDetail.setDrawBorders(true)

            cscStockDetail.setBorderColor(resources.getColor(R.color.medium_gray))

            val yAxisCandle: YAxis = cscStockDetail.axisLeft
            val rightAxis: YAxis = cscStockDetail.axisRight
            yAxisCandle.setDrawGridLines(false)
            rightAxis.setDrawGridLines(false)
            cscStockDetail.requestDisallowInterceptTouchEvent(true)

            val xAxisCandle: XAxis = cscStockDetail.xAxis

            xAxisCandle.setDrawGridLines(false) // disable x axis grid lines

            xAxisCandle.setDrawLabels(false)
            rightAxis.textColor = Color.WHITE
            yAxisCandle.setDrawLabels(false)
            xAxisCandle.granularity = 1f
            xAxisCandle.isGranularityEnabled = true
            xAxisCandle.setAvoidFirstLastClipping(true)

            val l: Legend = cscStockDetail.legend
            l.isEnabled = false
        }
    }
}