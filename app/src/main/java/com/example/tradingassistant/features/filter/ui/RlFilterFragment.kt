package com.example.tradingassistant.features.filter.ui

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.NavigationUI
import androidx.navigation.ui.setupWithNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.tradingassistant.R
import com.example.tradingassistant.core.data.EventObserver
import com.example.tradingassistant.core.data.Result
import com.example.tradingassistant.core.utils.onBackPressInBaseFragments
import com.example.tradingassistant.databinding.FragmentRlFiltersBinding
import com.example.tradingassistant.features.filter.data.RlFilterModel
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.snackbar.Snackbar

class RlFilterFragment : Fragment(), RlFilterListRecyclerViewAdapter.RlFilterItemCallBack {
    private lateinit var binding: FragmentRlFiltersBinding
    private val sharedViewModel: FiltersViewModel by activityViewModels()
    lateinit var recyclerViewAdapter: RlFilterListRecyclerViewAdapter


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_rl_filters, container, false)

        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (sharedViewModel.rlFiltersList.value !is Result.Success)
            sharedViewModel.getRlFilters()
        requireActivity().onBackPressedDispatcher.addCallback(
            viewLifecycleOwner,
            backPressedCallback
        )

        sharedViewModel.rlFilteredStocksList.observe(viewLifecycleOwner, EventObserver {
            when (it) {
                is Result.Error -> {
                    Log.d("Alireza", it.message)
                    binding.clLoadingStateRlFiltersFragment.visibility = View.GONE
                    Snackbar.make(binding.root, "Cannot Load the Data.", Snackbar.LENGTH_SHORT)
                        .show()
                }
                is Result.Loading -> binding.clLoadingStateRlFiltersFragment.visibility =
                    View.VISIBLE

                is Result.Success -> {
                    binding.clLoadingStateRlFiltersFragment.visibility = View.GONE

                    Snackbar.make(binding.root, "Data Loaded Successfully", Snackbar.LENGTH_SHORT)
                        .show()
                    findNavController().navigate(RlFilterFragmentDirections.actionRlFilterFragmentToRlFilteredStocksListFragment())
                }
            }
        })

        sharedViewModel.rlFiltersList.observe(viewLifecycleOwner, {
            when (it) {
                is Result.Success -> {
                    binding.clLoadingStateRlFiltersFragment.visibility = View.GONE
                    recyclerViewAdapter.submitList(it.data.filters)

                }
                is Result.Error -> {
                    binding.clLoadingStateRlFiltersFragment.visibility = View.GONE
                    Snackbar.make(binding.root, it.message, Snackbar.LENGTH_SHORT).show()
                }
                is Result.Loading -> {
                    binding.clLoadingStateRlFiltersFragment.visibility = View.VISIBLE
                }
            }
        })



        binding.apply {

            (bnvRlFilterFragment as BottomNavigationView).setupWithNavController(
                findNavController()
            )
            (bnvRlFilterFragment as BottomNavigationView).menu.getItem(3).isEnabled = false

            val appBarConfiguration = AppBarConfiguration.Builder(R.id.rlFilterFragment).build()
            NavigationUI.setupWithNavController(
                tbRlFilterFragment,
                findNavController(),
                appBarConfiguration
            )

            rvRlFilterList.setHasFixedSize(true)
            rvRlFilterList.layoutManager =
                LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
            recyclerViewAdapter = RlFilterListRecyclerViewAdapter(this@RlFilterFragment)
            rvRlFilterList.adapter = recyclerViewAdapter
        }
    }

    var mBackPressed: Long = 0
    private val backPressedCallback = object : OnBackPressedCallback(true) {
        override fun handleOnBackPressed() {
            mBackPressed = onBackPressInBaseFragments(mBackPressed)
        }
    }

    override fun onMostBtnClick(item: RlFilterModel) {
        sharedViewModel.rlFilterAscending(item.enName)
    }

    override fun onLeastBtnClick(item: RlFilterModel) {
        sharedViewModel.rlFilterDescending(item.enName)
    }


}