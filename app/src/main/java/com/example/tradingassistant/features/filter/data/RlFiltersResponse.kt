package com.example.tradingassistant.features.filter.data

import com.google.gson.annotations.SerializedName

data class RlFiltersResponse(
    @SerializedName("status_code") val statusCode: Int,
    @SerializedName("status_text") val statusText: String,
    val filters: List<RlFilterModel>,
)
