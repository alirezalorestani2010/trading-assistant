package com.example.tradingassistant.features.stocks.data

import com.example.tradingassistant.core.data.Result
import com.example.tradingassistant.core.data.RetrofitFactory
import retrofit2.Retrofit

class StocksRemoteDataSource {
    private val retrofit: Retrofit by lazy { RetrofitFactory.retrofit }
    private val service: StockService by lazy {
        retrofit.create(StockService::class.java)
    }

    suspend fun getStocks(): Result<StocksResponse>? {
        return try {
            val response = service.getStocks()
            if (response.isSuccessful) {
                response.body()?.let {
                    Result.Success(it)
                }
            } else
                Result.Error(response.message())

        } catch (e: Exception) {
            Result.Error(e.message ?: "EX")
        }
    }

    suspend fun getStockDetail(stockId: String): Result<StockDetailResponse> {
        try {
            val response = service.getStockDetail(stockId)
            if (response.isSuccessful) {
                response.body()?.let {
                    return Result.Success(it)
                }
            } else
                return Result.Error(response.message())

        } catch (e: Exception) {
            return Result.Error(e.message ?: "EX")
        }
        return Result.Error("EX")

    }


    suspend fun getVp(stockId: String, period: Int): Result<VpResponse> {
        try {
            val response = service.getVp(stockId, period)
            if (response.isSuccessful) {
                response.body()?.let {
                    return Result.Success(it)
                }
            } else
                return Result.Error(response.message())

        } catch (e: Exception) {
            return Result.Error(e.message ?: "EX")
        }
        return Result.Error("EX")

    }

}
