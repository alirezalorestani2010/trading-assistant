package com.example.tradingassistant.features.stocks.data

import com.google.gson.annotations.SerializedName

data class StocksResponse(
    @SerializedName("status_code")
    var statusCode: Int? = null,
    @SerializedName("status_text")
    var statusText: String? = null,
    var result: ArrayList<StocksModel>
)