package com.example.tradingassistant.features.stocks.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.tradingassistant.R
import com.example.tradingassistant.databinding.ItemStocksListBinding
import com.example.tradingassistant.features.stocks.data.StocksModel

class StocksListRecyclerViewAdapter(
    private val callBack: StockItemCallBack
) :
    ListAdapter<StocksModel, StocksListRecyclerViewAdapter.RecyclerViewHolder>(object :
        DiffUtil.ItemCallback<StocksModel>() {
        override fun areItemsTheSame(oldItem: StocksModel, newItem: StocksModel): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: StocksModel, newItem: StocksModel): Boolean {
            return true
        }

    }) {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerViewHolder {
        val mBinding: ItemStocksListBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.item_stocks_list,
            parent,
            false
        )
        return RecyclerViewHolder(mBinding)
    }

    override fun onBindViewHolder(holder: RecyclerViewHolder, position: Int) {
        val item = getItem(position)
        holder.uBinding.stock = item
        holder.itemView.tag = item

        val view = holder.uBinding

        when (item.trend) {
            0 -> {
                holder.trendImageView.setImageResource(R.color.red)
            }
            1 -> {
                holder.trendImageView.setImageResource(R.color.green)
            }
            else -> {
                holder.trendImageView.setImageResource(R.color.dark_gray)
            }
        }

        view.clStocksListItem.setOnClickListener {
            callBack.onItemClick(item)
        }
    }

    interface StockItemCallBack {
        fun onItemClick(item: StocksModel)
    }


    inner class RecyclerViewHolder(val uBinding: ItemStocksListBinding) :
        RecyclerView.ViewHolder(uBinding.root){
            val trendImageView = uBinding.ivTrendItemStock
        }

}
