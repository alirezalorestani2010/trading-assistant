package com.example.tradingassistant.features.filter.data

import com.example.tradingassistant.features.stocks.data.StocksModel
import com.google.gson.annotations.SerializedName

data class FilteredStocksResponse(
    @SerializedName("status_code") val statusCode: Int,
    @SerializedName("status_text") val statusText: String,
    val stocks: List<StocksModel>
)
