package com.example.tradingassistant.features.filter.data.entity

data class FiltersWithParam(
    val filter: FilterEntity,
    val params: List<FilterParamEntity>
)