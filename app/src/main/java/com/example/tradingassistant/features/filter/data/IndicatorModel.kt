package com.example.tradingassistant.features.filter.data

data class IndicatorModel(
    val name: String,
    val params: List<String>
)
