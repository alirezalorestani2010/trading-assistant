package com.example.tradingassistant.features.stocks.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.tradingassistant.core.data.Event
import com.example.tradingassistant.core.data.Result
import com.example.tradingassistant.features.stocks.data.StockDetailResponse
import com.example.tradingassistant.features.stocks.data.StockRepository
import com.example.tradingassistant.features.stocks.data.StocksResponse
import com.example.tradingassistant.features.stocks.data.VpResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class StockViewModel : ViewModel() {
    private val stockRepository: StockRepository by lazy { StockRepository() }


    private val _stocksList:
            MutableLiveData<Result<StocksResponse>> = MutableLiveData()
    val stocksList: LiveData<Result<StocksResponse>>
        get() = _stocksList

    private val _stockDetail:
            MutableLiveData<Event<Result<StockDetailResponse>>> = MutableLiveData()
    val stockDetail: LiveData<Event<Result<StockDetailResponse>>>
        get() = _stockDetail

    private val _vp:
            MutableLiveData<Event<Result<VpResponse>>> = MutableLiveData()
    val vp: LiveData<Event<Result<VpResponse>>>
        get() = _vp


    fun getStocks() {
        _stocksList.value = Result.Loading
        viewModelScope.launch(Dispatchers.IO) {
            _stocksList.postValue(stockRepository.getStocks())
        }
    }

    fun getStockDetail(stockId: String) {
        _stockDetail.value = Event(Result.Loading)
        viewModelScope.launch(Dispatchers.IO) {
            _stockDetail.postValue(Event(stockRepository.getStockDetail(stockId)))
        }
    }

    fun getVp(stockId: String, period: Int) {
        _vp.value = Event(Result.Loading)
        viewModelScope.launch(Dispatchers.IO) {
            _vp.postValue(Event(stockRepository.getVp(stockId, period)))
        }

    }


}
