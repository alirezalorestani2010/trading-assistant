package com.example.tradingassistant.features.stocks.data

import com.google.gson.annotations.SerializedName

data class StocksModel(
    var id: String,
    var name: String? = null,
    var group: String? = null,
    var close: Int? = null,
    var trend: Int? = null,
    @SerializedName("group_id")
    var groupId: Int? = null
)
