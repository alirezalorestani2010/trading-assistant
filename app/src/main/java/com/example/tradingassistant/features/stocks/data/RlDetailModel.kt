package com.example.tradingassistant.features.stocks.data

import com.google.gson.annotations.SerializedName

data class RlDetailModel(
    @SerializedName("num_buy_real") val numBuyReal: String,
    @SerializedName("num_buy_legal") val numBuyLegal: String,
    @SerializedName("num_sell_real") val numSellReal: String,
    @SerializedName("num_sell_legal") val numSellLegal: String,
    @SerializedName("volume_buy_real") val volumeBuyReal: String,
    @SerializedName("volume_buy_legal") val volumeBuyLegal: String,
    @SerializedName("volume_sell_real") val volumeSellReal: String,
    @SerializedName("volume_sell_legal") val volumeSellLegal: String,
    @SerializedName("value_buy_real") val valueBuyReal: String,
    @SerializedName("value_buy_legal") val valueBuyLegal: String,
    @SerializedName("value_sell_real") val valueSellReal: String,
    @SerializedName("value_sell_legal") val valueSellLegal: String,
    @SerializedName("ShDate") val ShDate: String
)