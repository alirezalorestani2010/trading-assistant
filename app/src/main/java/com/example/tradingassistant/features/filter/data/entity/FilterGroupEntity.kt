package com.example.tradingassistant.features.filter.data.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "filter_group_table")
data class FilterGroupEntity(
    @PrimaryKey(autoGenerate = true) var groupId: Long = 0,
    val name: String,
    val insertDate: Long
)