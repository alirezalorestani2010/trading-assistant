package com.example.tradingassistant.features.filter.data

import com.example.tradingassistant.features.filter.data.entity.IndicatorsResponse
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

interface FilterService {

    @GET("indicators")
//    @GET("29b994d0-b755-47e7-bf5b-10949374d833")
    suspend fun getIndicators(): Response<IndicatorsResponse>


    @POST("filter")
    suspend fun searchForStocks(
        @Body indicatorsList: FiltersToBeSearchedModel
    ): Response<FilteredStocksResponse>


    @GET("get_rl_filters")
    suspend fun getRlFilters(): Response<RlFiltersResponse>

    @GET("sort/{filter_name}/1")
    suspend fun rlFilterDescending(
        @Path("filter_name") filterName: String
    ): Response<RlFilteredStocksResponse>


    @GET("sort/{filter_name}/0")
    suspend fun rlFilterAscending(
        @Path("filter_name") filterName: String
    ): Response<RlFilteredStocksResponse>

}
