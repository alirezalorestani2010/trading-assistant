package com.example.tradingassistant.features.filter.data.entity

data class FilterToBeSaved(
    val group:FilterGroupModel,
)
