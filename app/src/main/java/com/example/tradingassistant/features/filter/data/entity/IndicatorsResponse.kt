package com.example.tradingassistant.features.filter.data.entity

import com.example.tradingassistant.features.filter.data.IndicatorModel
import com.google.gson.annotations.SerializedName

data class IndicatorsResponse(
    @SerializedName("status_code")
    val statusCode: Int,
    @SerializedName("status_text")
    val statusText: String,
    val indicators: List<IndicatorModel>
    )