package com.example.tradingassistant.features.filter.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.NavigationUI
import com.example.tradingassistant.R
import com.example.tradingassistant.databinding.DialogFragmentSaveFilterBinding
import com.example.tradingassistant.features.filter.data.entity.FilterGroupModel
import com.example.tradingassistant.features.filter.data.entity.FilterModel
import com.example.tradingassistant.features.filter.data.entity.FilterToBeSaved
import com.google.android.material.snackbar.Snackbar

class SaveFilterDialogFragment : DialogFragment() {
    private lateinit var binding: DialogFragmentSaveFilterBinding
    private val sharedViewModel: FiltersViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.dialog_fragment_save_filter,
            container,
            false
        )

        return binding.root
    }

    override fun onStart() {
        super.onStart()
        dialog?.window?.setLayout(
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.apply {

            val appBarConfiguration =
                AppBarConfiguration.Builder(R.id.saveFilterDialogFragment).build()
            NavigationUI.setupWithNavController(
                tbSaveFilterDialogFragment,
                findNavController(),
                appBarConfiguration
            )

            btnSaveFilterDialogFragment.setOnClickListener {
                if (etFilterNameSaveFilterDialogFragment.text.toString().isEmpty()) {
                    etFilterNameSaveFilterDialogFragment.error = "نام فیلتر را وارد کنید."
                    return@setOnClickListener
                }
                val filterName = etFilterNameSaveFilterDialogFragment.text.toString()
                try {
                    val filterModels =
                        arrayListOf<FilterModel>()
                    for (filter in sharedViewModel.filtersList.value!!) {
                        filterModels.add(
                            FilterModel(
                                filter.indicator,
                                filter.comparator,
                                filter.operand,
                                filter.indicatorParamsToFilterParamsList()
                            )
                        )

                    }
                    sharedViewModel.saveFilters(
                        FilterToBeSaved(
                            FilterGroupModel(
                                filterName,
                                System.currentTimeMillis(),
                                filterModels
                            )
                        )
                    )
                    Snackbar.make(binding.root, "Filter Saved Successfully", Snackbar.LENGTH_SHORT)
                        .show()
                    this@SaveFilterDialogFragment.dismiss()
                } catch (e: Exception) {
                    Snackbar.make(binding.root, "Save Failed.", Snackbar.LENGTH_SHORT)
                        .show()
                }

            }

        }
    }
}