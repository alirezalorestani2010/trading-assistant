package com.example.tradingassistant.features.stocks.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.NavigationUI
import androidx.navigation.ui.setupWithNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.tradingassistant.R
import com.example.tradingassistant.core.data.EventObserver
import com.example.tradingassistant.core.data.Result
import com.example.tradingassistant.core.utils.onBackPressInBaseFragments
import com.example.tradingassistant.databinding.FragmentStocksListBinding
import com.example.tradingassistant.features.stocks.data.StocksModel
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.snackbar.Snackbar

class StocksListFragment : Fragment(), StocksListRecyclerViewAdapter.StockItemCallBack {
    private val sharedViewModel: StockViewModel by activityViewModels()
    private lateinit var binding: FragmentStocksListBinding
    private val TAG = this.javaClass.simpleName
    private val recyclerViewAdapter: StocksListRecyclerViewAdapter by lazy {
        StocksListRecyclerViewAdapter(
            this@StocksListFragment
        )
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_stocks_list, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        requireActivity().onBackPressedDispatcher.addCallback(
            viewLifecycleOwner,
            backPressedCallback
        )

        if (sharedViewModel.stocksList.value !is Result.Success)
            sharedViewModel.getStocks()
        sharedViewModel.stocksList.observe(viewLifecycleOwner, Observer {
            when (it) {
                is Result.Success -> {
                    binding.clLoadingStateStocksListFragment.visibility = View.GONE
                    recyclerViewAdapter.submitList(it.data.result)
                }
                is Result.Error -> {
                    binding.clLoadingStateStocksListFragment.visibility = View.GONE
                    Snackbar.make(binding.root, "Error while getting list", Snackbar.LENGTH_SHORT)
                        .show()
                }
                is Result.Loading -> {
                    binding.clLoadingStateStocksListFragment.visibility = View.VISIBLE
                }
            }
        })

        sharedViewModel.stockDetail.observe(viewLifecycleOwner, EventObserver {
            when (it) {
                is Result.Error -> {
                    binding.clLoadingStateStocksListFragment.visibility = View.GONE
                    Snackbar.make(binding.root, "Cannot Load the Detail.", Snackbar.LENGTH_SHORT)
                        .show()
                }
                is Result.Loading -> binding.clLoadingStateStocksListFragment.visibility =
                    View.VISIBLE

                is Result.Success -> {
                    binding.clLoadingStateStocksListFragment.visibility = View.GONE
                    findNavController().navigate(StocksListFragmentDirections.actionStocksListFragmentToStockDetailFragment())
                }
            }
        })

        binding.apply {

            (bnvStocksListFragment as BottomNavigationView).setupWithNavController(
                findNavController()
            )
            (bnvStocksListFragment as BottomNavigationView).menu.getItem(2).isEnabled = false


            rvStocksList.setHasFixedSize(true)
            rvStocksList.layoutManager =
                LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
            rvStocksList.adapter = recyclerViewAdapter

            val appBarConfiguration = AppBarConfiguration.Builder(R.id.stocksListFragment).build()
            NavigationUI.setupWithNavController(
                tbStocksListFragment,
                findNavController(),
                appBarConfiguration
            )
        }
    }

    override fun onItemClick(item: StocksModel) {
        sharedViewModel.getStockDetail(item.id)
    }


    var mBackPressed: Long = 0
    private val backPressedCallback = object : OnBackPressedCallback(true) {
        override fun handleOnBackPressed() {
            mBackPressed = onBackPressInBaseFragments(mBackPressed)
        }
    }
}