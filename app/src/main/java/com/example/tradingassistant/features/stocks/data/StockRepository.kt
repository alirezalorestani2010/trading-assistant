package com.example.tradingassistant.features.stocks.data

import com.example.tradingassistant.core.data.Result

class StockRepository {

    private val stocksRemoteDataSource: StocksRemoteDataSource by lazy {
        StocksRemoteDataSource()
    }

    suspend fun getStocks(): Result<StocksResponse>? = stocksRemoteDataSource.getStocks()
    suspend fun getStockDetail(stockId: String): Result<StockDetailResponse> =
        stocksRemoteDataSource.getStockDetail(stockId)

    suspend fun getVp(stockId: String, period: Int): Result<VpResponse> =
        stocksRemoteDataSource.getVp(stockId, period)

}
