package com.example.tradingassistant.features.stocks.data

import com.google.gson.annotations.SerializedName

data class VpResponse(
    @SerializedName("status_code") val statusCode: Int,
    @SerializedName("status_text") val statusText: String,
    val result: String
)
