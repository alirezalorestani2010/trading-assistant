package com.example.tradingassistant.features.stocks.ui

import android.content.pm.ActivityInfo
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.tradingassistant.R
import com.example.tradingassistant.core.data.Result
import com.example.tradingassistant.databinding.FragmentVpBinding


class VpFragment : Fragment() {
    private val sharedViewModel: StockViewModel by activityViewModels()
    private lateinit var binding: FragmentVpBinding
    private val TAG = this.javaClass.simpleName

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_vp, container, false)


        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        requireActivity().requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE

        val options: RequestOptions = RequestOptions()
            .fitCenter()
            .placeholder(R.mipmap.ic_launcher_round)
            .error(R.mipmap.ic_launcher_round)

        Glide.with(requireContext())
            .load("http://109.122.255.4:8000" + (sharedViewModel.vp.value?.peekContent() as Result.Success).data.result)
            .apply(options)
            .into(binding.ivVpFragment)
    }

    override fun onDestroy() {
        super.onDestroy()
        requireActivity().requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED

    }
}