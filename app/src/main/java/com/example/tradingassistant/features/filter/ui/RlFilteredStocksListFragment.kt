package com.example.tradingassistant.features.filter.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.tradingassistant.R
import com.example.tradingassistant.core.data.EventObserver
import com.example.tradingassistant.core.data.Result
import com.example.tradingassistant.databinding.FragmentStocksListBinding
import com.example.tradingassistant.features.stocks.data.StocksModel
import com.example.tradingassistant.features.stocks.ui.StockViewModel
import com.example.tradingassistant.features.stocks.ui.StocksListRecyclerViewAdapter
import com.google.android.material.snackbar.Snackbar

class RlFilteredStocksListFragment : Fragment(), StocksListRecyclerViewAdapter.StockItemCallBack {
    private lateinit var binding: FragmentStocksListBinding
    private val sharedViewModel: FiltersViewModel by activityViewModels()
    private val stocksViewModel: StockViewModel by activityViewModels()
    private val recyclerViewAdapter: StocksListRecyclerViewAdapter by lazy {
        StocksListRecyclerViewAdapter(
            this@RlFilteredStocksListFragment
        )
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_stocks_list, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.apply {

            bnvStocksListFragment.visibility=View.GONE

            rvStocksList.setHasFixedSize(true)
            rvStocksList.layoutManager =
                LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
            rvStocksList.adapter = recyclerViewAdapter
            recyclerViewAdapter.submitList((sharedViewModel.rlFilteredStocksList.value?.peekContent() as Result.Success).data.toStockModel())
        }



        stocksViewModel.stockDetail.observe(viewLifecycleOwner, EventObserver {
            when (it) {
                is Result.Error -> {
                    binding.clLoadingStateStocksListFragment.visibility = View.GONE
                    Snackbar.make(binding.root, "Cannot Load the Detail.", Snackbar.LENGTH_SHORT)
                        .show()
                }
                is Result.Loading -> binding.clLoadingStateStocksListFragment.visibility =
                    View.VISIBLE

                is Result.Success -> {
                    binding.clLoadingStateStocksListFragment.visibility = View.GONE
                    findNavController().navigate(RlFilteredStocksListFragmentDirections.actionRlFilteredStocksListFragmentToStockDetailFragment2())
                }
            }
        })
    }


    override fun onItemClick(item: StocksModel) {
        stocksViewModel.getStockDetail(item.id)
    }
}