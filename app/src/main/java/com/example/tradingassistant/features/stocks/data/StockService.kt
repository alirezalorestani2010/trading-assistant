package com.example.tradingassistant.features.stocks.data

import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

interface StockService {

    @GET("symbols")
//    @GET("d5c2779c-14aa-4b3b-b152-332887b35cde")
    suspend fun getStocks(): Response<StocksResponse>

    @GET("symbol/{stock_id}")
//    @GET("9cb0bb50-1c42-4475-b85d-cbe07c34d1ed")
    suspend fun getStockDetail(
        @Path("stock_id") stockId: String
    ): Response<StockDetailResponse>

    @GET("get_vp/{stock_id}/{period}")
    suspend fun getVp(
        @Path("stock_id") stockId: String,
        @Path("period") period: Int
    ): Response<VpResponse>
}
