package com.example.tradingassistant.features.news.data

import com.google.gson.annotations.SerializedName

data class NewsResponse(
    @SerializedName("status_code") val statusCode: Int,
    @SerializedName("status_text") val statusText: String,
    val result: ArrayList<NewsModel>
)
