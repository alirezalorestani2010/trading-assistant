package com.example.tradingassistant.features.news.data

import com.example.tradingassistant.core.data.Result

class NewsRepository {
    private val newsRemoteDataSource: NewsRemoteDataSource by lazy {
        NewsRemoteDataSource()
    }

    suspend fun getNews(): Result<NewsResponse>? = newsRemoteDataSource.getNews()

}
