package com.example.tradingassistant.features.filter.ui


import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.tradingassistant.R
import com.example.tradingassistant.core.data.EventObserver
import com.example.tradingassistant.core.data.Result
import com.example.tradingassistant.databinding.FragmentFiltersListBinding
import com.example.tradingassistant.features.filter.data.FiltersListRecyclerViewAdapter
import com.example.tradingassistant.features.filter.data.entity.*
import com.google.android.material.snackbar.Snackbar


class FiltersListFragment : Fragment() {
    private lateinit var binding: FragmentFiltersListBinding
    private val sharedViewModel: FiltersViewModel by activityViewModels()
    private val TAG = this.javaClass.simpleName
    private val recyclerViewAdapter: FiltersListRecyclerViewAdapter by lazy {
        FiltersListRecyclerViewAdapter()
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_filters_list, container, false)

        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        sharedViewModel.apply {
            filtersList.observe(viewLifecycleOwner, {
                Log.d(TAG, "${sharedViewModel.filtersList.value}")
                if (it.isEmpty())
                    binding.tvEmptyRecyclerView.visibility = View.VISIBLE
                else
                    binding.tvEmptyRecyclerView.visibility = View.INVISIBLE
                recyclerViewAdapter.submitList(it)
            })

            stocksList.observe(viewLifecycleOwner, EventObserver {
                when (it) {
                    is Result.Success -> {
                        findNavController().navigate(HomeFilterFragmentDirections.actionHomeFilterFragmentToFilteredStocksFragment())
                        binding.clLoadingStateFiltersListFragment.visibility = View.GONE
                    }
                    is Result.Error -> {
                        binding.clLoadingStateFiltersListFragment.visibility = View.GONE
                        Snackbar.make(
                            binding.root,
                            "خطایی رخ داده است.",
                            Snackbar.LENGTH_SHORT
                        ).setAction("Retry") { searchForStocks() }.show()
                    }
                    is Result.Loading -> {
                        binding.clLoadingStateFiltersListFragment.visibility = View.VISIBLE
                        Snackbar.make(
                            binding.root,
                            "Loading",
                            Snackbar.LENGTH_SHORT
                        ).show()
                    }
                }
            })

        }



        binding.apply {
            btnNavigateToAddFiltersFiltersListFragment.setOnClickListener {
                if (sharedViewModel.selectedIndicator.value != null)
                    sharedViewModel.resetIndicator()
                findNavController().navigate(HomeFilterFragmentDirections.actionHomeFilterFragmentToAddFilterBottomSheetDialogFragment())
            }
            btnSearchStocksFiltersListFragment.setOnClickListener {
                if (sharedViewModel.filtersList.value?.size == 0) {
                    Snackbar.make(binding.root, "Add One Filter at least.", Snackbar.LENGTH_SHORT)
                        .show()
                    return@setOnClickListener
                }
                sharedViewModel.searchForStocks()
            }

            btnSaveFilterFiltersListFragment.setOnClickListener {
                if (sharedViewModel.filtersList.value?.size == 0) {
                    Snackbar.make(binding.root, "Add One Filter at least.", Snackbar.LENGTH_SHORT)
                        .show()
                    return@setOnClickListener
                }
                findNavController().navigate(HomeFilterFragmentDirections.actionHomeFilterFragmentToSaveFilterDialogFragment())
            }




            rvFiltersList.setHasFixedSize(true)
            rvFiltersList.layoutManager =
                LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
            rvFiltersList.adapter = recyclerViewAdapter

            ItemTouchHelper(object : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
                override fun onMove(
                    recyclerView: RecyclerView,
                    viewHolder: RecyclerView.ViewHolder,
                    target: RecyclerView.ViewHolder
                ): Boolean {
                    return false
                }

                override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                    val position = viewHolder.adapterPosition
                    val deletedFilter =
                        sharedViewModel.filtersList.value?.get(position)
                    val newList = sharedViewModel.filtersList.value?.toMutableList()
                    newList?.remove(deletedFilter)
                    newList?.toList()?.let { sharedViewModel.setFiltersList(it) }

                    Snackbar.make(rvFiltersList, "Filter Deleted!", Snackbar.LENGTH_SHORT)
                        .setAction(
                            "Undo"
                        ) {
                            deletedFilter?.let { it1 ->
                                newList?.add(
                                    position,
                                    it1
                                )
                                newList?.toList()?.let { sharedViewModel.setFiltersList(it) }
                            }
                        }.show()
                }

            }).attachToRecyclerView(rvFiltersList)
        }

    }
}