package com.example.tradingassistant.features.filter.data.entity

data class FilterModel(
    val indicatorName: String,
    val comparator: String,
    val operand: Float,
    val params: List<FilterParamModel>
) {
    fun toFilterEntity(filterGroupId: Long): FilterEntity =
        FilterEntity(indicatorName, filterGroupId, comparator, operand)
}
