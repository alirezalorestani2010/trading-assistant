package com.example.tradingassistant.features.filter.ui


import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.tradingassistant.R
import com.example.tradingassistant.databinding.ItemSavedFiltersListBinding
import com.example.tradingassistant.features.filter.data.entity.FiltersWithFilterGroupWithParams

class SavedFiltersListRecyclerViewAdapter(
    private val callBack: SavedFiltersCallback
) : ListAdapter<FiltersWithFilterGroupWithParams, SavedFiltersListRecyclerViewAdapter.RecyclerViewHolder>(
    object :
        DiffUtil.ItemCallback<FiltersWithFilterGroupWithParams>() {
        override fun areItemsTheSame(
            oldItem: FiltersWithFilterGroupWithParams,
            newItem: FiltersWithFilterGroupWithParams
        ): Boolean {
            return oldItem.filterGroup.name == newItem.filterGroup.name
        }

        override fun areContentsTheSame(
            oldItem: FiltersWithFilterGroupWithParams,
            newItem: FiltersWithFilterGroupWithParams
        ): Boolean {
            return true
        }

    }) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerViewHolder {
        val mBinding: ItemSavedFiltersListBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.item_saved_filters_list,
            parent,
            false
        )
        return RecyclerViewHolder(mBinding)
    }

    override fun onBindViewHolder(holder: RecyclerViewHolder, position: Int) {
        val item = getItem(position)
        holder.uBinding.savedFilter = item
        holder.itemView.tag = item

        val view = holder.uBinding

        var paramText = ""
        for (filter in item.filtersWithParam) {
            paramText += "${filter.filter.indicatorName} ${filter.filter.comparator} ${filter.filter.operand}\n"
        }

        view.tvParamsItemSavedFiltersList.text = paramText

    }

    interface SavedFiltersCallback {
        fun onClick(item: FiltersWithFilterGroupWithParams)
    }


    inner class RecyclerViewHolder(val uBinding: ItemSavedFiltersListBinding) :
        RecyclerView.ViewHolder(uBinding.root)

}

