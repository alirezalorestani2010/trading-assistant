package com.example.tradingassistant.features.filter.data.entity

import com.google.gson.annotations.SerializedName

data class FilterResponse(
    @SerializedName("status_code")
    val statusCode: Int,
    @SerializedName("status_text")
    val statusText: String,
    val result: List<String>
)
