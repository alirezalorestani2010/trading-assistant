package com.example.tradingassistant.features.filter.data.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "filter_param_table")
data class FilterParamEntity(
    @PrimaryKey(autoGenerate = true) var paramId: Long = 0,
    val paramName: String,
    val paramValue: Float,
    val filterId: Long
)
