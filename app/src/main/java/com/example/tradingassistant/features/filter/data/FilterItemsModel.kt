package com.example.tradingassistant.features.filter.data

import com.example.tradingassistant.features.filter.data.entity.FilterParamModel
import com.google.gson.annotations.SerializedName

data class FilterItemsModel(
    @Transient
    val id: Int,
    val indicator: String,
    @SerializedName("params")
    val indicatorParams: ArrayList<IndicatorParam>,
    val comparator: String,
    val operand: Float,
    @Transient
    var expanded: Boolean = false
) {

    fun indicatorParamsToFilterParamsList(): ArrayList<FilterParamModel> {
        return ArrayList(this.indicatorParams.map { FilterParamModel(it.name, it.value) })
    }

}
