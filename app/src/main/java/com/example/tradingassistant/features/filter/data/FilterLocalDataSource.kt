package com.example.tradingassistant.features.filter.data

import com.example.tradingassistant.core.data.TradingAssistantDataBase
import com.example.tradingassistant.features.filter.data.entity.FilterToBeSaved
import com.example.tradingassistant.features.filter.data.entity.FiltersWithFilterGroupWithParams

class FilterLocalDataSource {
    private val dao: FilterDao = TradingAssistantDataBase.getDatabase()?.filterDao()!!

    suspend fun insertAllFilters(filterToBeSaved: FilterToBeSaved) {
        dao.insertAllFilters(filterToBeSaved)
    }

    suspend fun getSavedFilters(): List<FiltersWithFilterGroupWithParams> {
        return dao.getAllSavedFilters()
    }


}