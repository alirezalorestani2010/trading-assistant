package com.example.tradingassistant.features.filter.ui

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter

class FilterViewPagerAdapter(fragment: Fragment) : FragmentStateAdapter(fragment) {
    override fun getItemCount(): Int {
        return 2
    }

    override fun createFragment(position: Int): Fragment {
        return when (position) {
            0 -> FiltersListFragment()
            1 -> SavedFiltersFragment()
            else -> FiltersListFragment()
        }
    }
}