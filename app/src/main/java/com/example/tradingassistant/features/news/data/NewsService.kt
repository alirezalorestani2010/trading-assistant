package com.example.tradingassistant.features.news.data

import retrofit2.Response
import retrofit2.http.GET

interface NewsService {

    @GET("news")
//    @GET("b3f14dc1-6bd6-46a9-936b-6c70c43713e9")
    suspend fun getNews(): Response<NewsResponse>

}
