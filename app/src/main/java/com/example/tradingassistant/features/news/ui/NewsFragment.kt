package com.example.tradingassistant.features.news.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.NavigationUI
import androidx.navigation.ui.setupWithNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.tradingassistant.R
import com.example.tradingassistant.core.data.Result
import com.example.tradingassistant.core.utils.onBackPressInBaseFragments
import com.example.tradingassistant.databinding.FragmentNewsBinding
import com.example.tradingassistant.features.news.data.NewsModel
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.snackbar.Snackbar

class NewsFragment : Fragment(), NewsListRecyclerViewAdapter.CallBack {
    private lateinit var binding: FragmentNewsBinding
    private lateinit var recyclerViewAdapter: NewsListRecyclerViewAdapter
    private lateinit var viewModel: NewsViewModel


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_news, container, false)
        viewModel = ViewModelProvider(this).get(NewsViewModel::class.java)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        requireActivity().onBackPressedDispatcher.addCallback(
            viewLifecycleOwner,
            backPressedCallback
        )


        if (viewModel.newsList.value !is Result.Success)
            viewModel.getNews()
        viewModel.newsList.observe(viewLifecycleOwner, Observer {
            when (it) {
                is Result.Success -> {
                    binding.clLoadingStateNewsFragment.visibility = View.GONE
                    recyclerViewAdapter.submitList(it.data.result)
                }
                is Result.Error -> {
                    binding.clLoadingStateNewsFragment.visibility = View.GONE
                    Snackbar.make(binding.root, "Error while getting list", Snackbar.LENGTH_SHORT)
                        .show()
                }
                is Result.Loading -> {
                    binding.clLoadingStateNewsFragment.visibility = View.VISIBLE
                }
            }
        })
        binding.apply {
            (bnvNewsFragment as BottomNavigationView).setupWithNavController(
                findNavController()
            )
            (bnvNewsFragment as BottomNavigationView).menu.getItem(1).isEnabled = false

            rvNewsList.setHasFixedSize(true)
            rvNewsList.layoutManager =
                LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
            recyclerViewAdapter = NewsListRecyclerViewAdapter(this@NewsFragment)
            rvNewsList.adapter = recyclerViewAdapter


            val appBarConfiguration = AppBarConfiguration.Builder(R.id.newsFragment).build()
            NavigationUI.setupWithNavController(
                tbNewsFragment,
                findNavController(),
                appBarConfiguration
            )
        }
    }

    override fun onItemClick(item: NewsModel) {
//        Toast.makeText(requireContext(), "news ${item.id}", Toast.LENGTH_SHORT).show()
    }

    var mBackPressed: Long = 0
    private val backPressedCallback = object : OnBackPressedCallback(true) {
        override fun handleOnBackPressed() {
            mBackPressed = onBackPressInBaseFragments(mBackPressed)
        }
    }
}