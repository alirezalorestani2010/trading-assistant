package com.example.tradingassistant.features.filter.ui

import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.EditText
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.activityViewModels
import com.example.tradingassistant.R
import com.example.tradingassistant.core.data.Result
import com.example.tradingassistant.databinding.DialogFragmentAddFilterBinding
import com.example.tradingassistant.features.filter.data.FilterItemsModel
import com.example.tradingassistant.features.filter.data.IndicatorParam
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.android.material.snackbar.Snackbar


class AddFilterBottomSheetDialogFragment : BottomSheetDialogFragment() {
    private lateinit var binding: DialogFragmentAddFilterBinding
    private val sharedViewModel: FiltersViewModel by activityViewModels()
    private val TAG = this.javaClass.simpleName


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.dialog_fragment_add_filter, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        sharedViewModel.setComparator(getString(R.string.equal))

        if (sharedViewModel.indicatorsList.value !is Result.Success)
            sharedViewModel.getIndicators()
        binding.apply {

            btnAddFilterAddFilterDialogFragment.setOnClickListener {
                if (isIndicatorSelected() && isSecondOperandValid() && areIndicatorParamsFilled()) {
                    val indicatorParams = arrayListOf<IndicatorParam>()
                    indicatorParamInputLayouts.forEachIndexed { index, view ->
                        val paramEditText =
                            view.findViewById<EditText>(R.id.et_indicator_param_value)
                        try {
                            val paramName =
                                (sharedViewModel.indicatorsList.value as Result.Success).data.indicators[sharedViewModel.selectedIndicator.value?.second
                                    ?: 0].params[index]
                            val paramValue = paramEditText.text.toString().toFloat()
                            val indicatorParam = IndicatorParam(
                                paramName,
                                paramValue
                            )
                            indicatorParams.add(
                                indicatorParam
                            )
                            Log.d(TAG, "$indicatorParam")
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }
                    sharedViewModel.filtersList.value?.let { it1 ->
                        sharedViewModel.setFiltersList(
                            it1.plus(
                                FilterItemsModel(
                                    it1.size,
                                    sharedViewModel.selectedIndicator.value?.first ?: "",
                                    indicatorParams,
                                    sharedViewModel.comparator.value ?: "",
                                    sharedViewModel.secondOperand.value?.toFloat() ?: 0f
                                )
                            )
                        )
                    }
                    this@AddFilterBottomSheetDialogFragment.dismiss()
                }
            }
            spIndicatorsList.setOnItemClickListener { parent, _, position, _ ->
                val indicatorName = parent.getItemAtPosition(position).toString()
                Log.d(TAG, "indicator: $indicatorName, position:$position")
                sharedViewModel.setIndicator(Pair(indicatorName, position))
            }
            sharedViewModel.selectedIndicator.observe(viewLifecycleOwner, {
                binding.llParamsContainerAddFiltersFragment.removeAllViews()
                indicatorParamInputLayouts.clear()
                if (sharedViewModel.indicatorsList.value is Result.Success && sharedViewModel.selectedIndicator.value != null) {
                    val selectedIndicator =
                        (sharedViewModel.indicatorsList.value as Result.Success).data.indicators[it.second]
                    for (param in selectedIndicator.params) {
                        addIndicatorParamInputLayout(param)
                    }
                }
            })
            sharedViewModel.indicatorsList.observe(viewLifecycleOwner, {
                when (it) {
                    is Result.Success -> {
                        clAddFilterDialogFragment.isClickable = true
                        binding.clLoadingStateAddFilterDialogFragment.visibility = View.GONE
                        val indicators = it.data.indicators
                        val arrayAdapter = ArrayAdapter(
                            requireContext(),
                            R.layout.support_simple_spinner_dropdown_item,
                            indicators.map { indicatorModel -> indicatorModel.name }
                        )
                        arrayAdapter.setDropDownViewResource(
                            android.R.layout.simple_spinner_dropdown_item
                        )
                        spIndicatorsList.setAdapter(arrayAdapter)
                    }
                    is Result.Error -> {
                        clAddFilterDialogFragment.isClickable = true
                        binding.clLoadingStateAddFilterDialogFragment.visibility = View.GONE
                        Snackbar.make(
                            binding.root,
                            "Error while getting indicators",
                            Snackbar.LENGTH_SHORT
                        ).setAction("Retry") { sharedViewModel.getIndicators() }.show()
                    }
                    is Result.Loading -> {
                        clAddFilterDialogFragment.isClickable = false
                        binding.clLoadingStateAddFilterDialogFragment.visibility = View.VISIBLE

                    }
                }
            })


            btnEqualTo.setOnClickListener {
                sharedViewModel.setComparator(getString(R.string.equal))
            }
            btnGreaterThan.setOnClickListener {
                sharedViewModel.setComparator(getString(R.string.greater))
            }
            btnLessThan.setOnClickListener {
                sharedViewModel.setComparator(getString(R.string.less))
            }
            etComparisonOperand2AddFiltersFragment.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(
                    s: CharSequence?,
                    start: Int,
                    count: Int,
                    after: Int
                ) {
//                    TODO("Not yet implemented")
                }

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
//                    TODO("Not yet implemented")
                }

                override fun afterTextChanged(s: Editable?) {
                    sharedViewModel.setSecondOperand(s.toString())
                }

            })
        }
    }

    private val indicatorParamInputLayouts = arrayListOf<View>()


    private fun addIndicatorParamInputLayout(paramLabel: String) {
        val inflater =
            requireActivity().applicationContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.layout_indicator_param, null)
        val paramLabelTextView = view.findViewById<TextView>(R.id.tv_indicator_param_label)
        paramLabelTextView.text = paramLabel
        binding.llParamsContainerAddFiltersFragment.addView(
            view,
            indicatorParamInputLayouts.size,
            ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
        )
        indicatorParamInputLayouts.add(view)
    }

    private fun areIndicatorParamsFilled(): Boolean {
        indicatorParamInputLayouts.forEach {
            val textView = it.findViewById<EditText>(R.id.et_indicator_param_value)
            if (textView.text.toString().isEmpty()) {
                textView.error = "Please fill this parameter!"
                return false
            }
        }
        return true
    }

    private fun isIndicatorSelected(): Boolean {
        if (binding.spIndicatorsList.text.toString() == resources.getString(R.string.choose_indicator)) {
            binding.spIndicatorsList.error = "please select an indicator!"
            return false
        }
        return true
    }


    private fun isSecondOperandValid(): Boolean {
        if (binding.etComparisonOperand2AddFiltersFragment.text.toString().isEmpty()) {
            binding.etComparisonOperand2AddFiltersFragment.error = "Please fill the operand!"
            return false
        }
        return true
    }
}
