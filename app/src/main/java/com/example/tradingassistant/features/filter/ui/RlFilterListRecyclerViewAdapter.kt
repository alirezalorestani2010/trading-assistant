package com.example.tradingassistant.features.filter.ui


import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.tradingassistant.R
import com.example.tradingassistant.databinding.ItemRlFilterListBinding
import com.example.tradingassistant.features.filter.data.RlFilterModel

class RlFilterListRecyclerViewAdapter(
    private val callBack: RlFilterItemCallBack
) : ListAdapter<RlFilterModel, RlFilterListRecyclerViewAdapter.RecyclerViewHolder>(object :
    DiffUtil.ItemCallback<RlFilterModel>() {
    override fun areItemsTheSame(oldItem: RlFilterModel, newItem: RlFilterModel): Boolean {
        return oldItem.enName == newItem.enName
    }

    override fun areContentsTheSame(oldItem: RlFilterModel, newItem: RlFilterModel): Boolean {
        return true
    }

}) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerViewHolder {
        val mBinding: ItemRlFilterListBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.item_rl_filter_list,
            parent,
            false
        )
        return RecyclerViewHolder(mBinding)
    }

    override fun onBindViewHolder(holder: RecyclerViewHolder, position: Int) {
        val item = getItem(position)
        holder.uBinding.rlFilter = item
        holder.itemView.tag = item

        val view = holder.uBinding

        view.btnMostItemRlFilterList.setOnClickListener {
            callBack.onMostBtnClick(item)
        }
        view.btnLeastItemRlFilterList.setOnClickListener {
            callBack.onLeastBtnClick(item)
        }
    }

    interface RlFilterItemCallBack {
        fun onMostBtnClick(item: RlFilterModel)
        fun onLeastBtnClick(item: RlFilterModel)
    }


    inner class RecyclerViewHolder(val uBinding: ItemRlFilterListBinding) :
        RecyclerView.ViewHolder(uBinding.root)

}

