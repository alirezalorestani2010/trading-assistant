package com.example.tradingassistant.features.filter.data

import com.google.gson.annotations.SerializedName

data class StocksModelInRlFilter(
    @SerializedName("stock_id") val stockId: String,
    @SerializedName("num_buy_real") val numByReal: Float,
    val details: StocksDetailModelInRlFilter
)
