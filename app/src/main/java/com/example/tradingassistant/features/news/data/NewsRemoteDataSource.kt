package com.example.tradingassistant.features.news.data

import com.example.tradingassistant.core.data.Result
import com.example.tradingassistant.core.data.RetrofitFactory
import retrofit2.Retrofit

class NewsRemoteDataSource {

    private val retrofit: Retrofit by lazy { RetrofitFactory.retrofit }
    private val service: NewsService by lazy {
        retrofit.create(NewsService::class.java)
    }
    suspend fun getNews(): Result<NewsResponse>? {
        return try {
            val response = service.getNews()
            if (response.isSuccessful) {
                response.body()?.let {
                    Result.Success(it)
                }
            } else
                Result.Error(response.message())

        } catch (e: Exception) {
            Result.Error(e.message ?: "EX")
        }
    }


}
