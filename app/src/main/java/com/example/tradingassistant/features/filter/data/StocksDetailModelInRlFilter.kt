package com.example.tradingassistant.features.filter.data

import com.google.gson.annotations.SerializedName

data class StocksDetailModelInRlFilter(
    val id: String,
    val name: String,
    val group: String,
    val close: Int,
    val trend: Int,
    @SerializedName("group_id") val groupId: Int
)
