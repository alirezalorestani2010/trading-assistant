package com.example.tradingassistant.features.filter.data

import com.google.gson.annotations.SerializedName

data class FiltersToBeSearchedModel(
    @SerializedName("indicators_list")
    val indicatorsList: List<FilterItemsModel>,
    val period: Int = 10
)