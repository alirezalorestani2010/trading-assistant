package com.example.tradingassistant.features.news.data

data class NewsModel(
    val time:String,
    val desc: String
)
