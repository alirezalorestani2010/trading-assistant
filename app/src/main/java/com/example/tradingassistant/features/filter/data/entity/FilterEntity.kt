package com.example.tradingassistant.features.filter.data.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "saved_filters_table")
data class FilterEntity(
    val indicatorName: String,
    val filterGroupId: Long,
    val comparator: String,
    val operand: Float,
    @PrimaryKey(autoGenerate = true) var filterId: Long = 0
)
