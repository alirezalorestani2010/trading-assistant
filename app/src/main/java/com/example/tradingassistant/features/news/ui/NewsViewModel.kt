package com.example.tradingassistant.features.news.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.tradingassistant.core.data.Result
import com.example.tradingassistant.features.news.data.NewsRepository
import com.example.tradingassistant.features.news.data.NewsResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class NewsViewModel : ViewModel() {
    private val newsRepository: NewsRepository by lazy { NewsRepository() }

    private val _newsList:
            MutableLiveData<Result<NewsResponse>> = MutableLiveData()
    val newsList: LiveData<Result<NewsResponse>>
        get() = _newsList


    fun getNews() {
        _newsList.value = Result.Loading
        viewModelScope.launch(Dispatchers.IO) {
            _newsList.postValue(newsRepository.getNews())
        }
    }
}