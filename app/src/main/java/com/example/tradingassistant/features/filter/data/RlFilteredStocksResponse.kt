package com.example.tradingassistant.features.filter.data

import com.example.tradingassistant.features.stocks.data.StocksModel
import com.google.gson.annotations.SerializedName

data class RlFilteredStocksResponse(
    @SerializedName("status_code") val statusCode: Int,
    @SerializedName("status_text") val statusText: String,
    val stocks: List<StocksModelInRlFilter>
) {

    fun toStockModel(): ArrayList<StocksModel> {
        return ArrayList(this.stocks.map {
            StocksModel(
                it.stockId,
                it.details.name,
                it.details.group,
                it.details.close,
                it.details.trend,
                it.details.groupId
            )
        })
    }
}
