package com.example.tradingassistant.features.stocks.data

import com.github.mikephil.charting.data.BarEntry
import com.github.mikephil.charting.data.CandleEntry
import com.google.gson.annotations.SerializedName

data class StockDetailResponse(
    @SerializedName("status_code")
    val statusCode: Int,
    @SerializedName("status_text")
    val statusText: String,
    @SerializedName("stock_id")
    val stockId: String,
    val result: ArrayList<CandleModel>,
    val vp: ArrayList<VpModel>,
    val detail: StockDetailModel

) {

    fun toCandleEntry(): ArrayList<CandleEntry> {
        return ArrayList(this.result.mapIndexed { index, it ->
            CandleEntry(index.toFloat(), it.High, it.Low, it.Open, it.Close)
        })
    }

    fun toBarEntry(): ArrayList<BarEntry> {
        return ArrayList(this.vp.map {
            BarEntry(it.x, it.y)
        })
    }
}