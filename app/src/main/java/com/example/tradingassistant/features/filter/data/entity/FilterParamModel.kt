package com.example.tradingassistant.features.filter.data.entity

data class FilterParamModel(
    val paramName: String,
    val paramValue: Float
) {
    fun toFilterParamEntity(filterId: Long): FilterParamEntity =
        FilterParamEntity(paramName = paramName, paramValue = paramValue, filterId = filterId)
}
