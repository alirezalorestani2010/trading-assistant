package com.example.tradingassistant.features.filter.ui

import androidx.lifecycle.ViewModelProvider
import com.example.tradingassistant.features.filter.data.FiltersRepository

class FiltersViewModelFactory(private val filtersRepository: FiltersRepository):ViewModelProvider.NewInstanceFactory()