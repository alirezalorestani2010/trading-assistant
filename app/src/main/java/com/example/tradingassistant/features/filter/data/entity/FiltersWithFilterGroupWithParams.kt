package com.example.tradingassistant.features.filter.data.entity

data class FiltersWithFilterGroupWithParams(
    val filterGroup: FilterGroupEntity,
    val filtersWithParam: List<FiltersWithParam>,
)
