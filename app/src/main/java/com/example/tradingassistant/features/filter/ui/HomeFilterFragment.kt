package com.example.tradingassistant.features.filter.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.NavigationUI
import androidx.navigation.ui.setupWithNavController
import com.example.tradingassistant.R
import com.example.tradingassistant.core.utils.onBackPressInBaseFragments
import com.example.tradingassistant.databinding.FragmentHomeFilterBinding
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.tabs.TabLayoutMediator

class HomeFilterFragment : Fragment() {
    private lateinit var binding: FragmentHomeFilterBinding
    private val sharedViewModel: FiltersViewModel by activityViewModels()
    private val TAG = this.javaClass.simpleName


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_home_filter, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        requireActivity().onBackPressedDispatcher.addCallback(
            viewLifecycleOwner,
            backPressedCallback
        )
        binding.apply {
            vpFilters.adapter = FilterViewPagerAdapter(this@HomeFilterFragment)
            TabLayoutMediator(tlFilters, vpFilters) { tab, position ->
                tab.text = when (position) {
                    0 -> "فیلتر جدید"
                    1 -> "فیلترهای ذخیره شده"
                    else -> "فیلتر جدید"
                }
            }.attach()


            (bnvHomeFilters as BottomNavigationView).setupWithNavController(
                findNavController()
            )
            (bnvHomeFilters as BottomNavigationView).menu.getItem(0).isEnabled = false


            val appBarConfiguration = AppBarConfiguration.Builder(R.id.homeFilterFragment).build()
            NavigationUI.setupWithNavController(
                tbHomeFilterFragment,
                findNavController(),
                appBarConfiguration
            )

        }
    }

    var mBackPressed: Long = 0
    private val backPressedCallback = object : OnBackPressedCallback(true) {
        override fun handleOnBackPressed() {
            mBackPressed = onBackPressInBaseFragments(mBackPressed)
        }
    }
}