package com.example.tradingassistant.features.filter.ui

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.tradingassistant.core.data.Event
import com.example.tradingassistant.core.data.Result
import com.example.tradingassistant.features.filter.data.*
import com.example.tradingassistant.features.filter.data.entity.FilterToBeSaved
import com.example.tradingassistant.features.filter.data.entity.FiltersWithFilterGroupWithParams
import com.example.tradingassistant.features.filter.data.entity.IndicatorsResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


class FiltersViewModel : ViewModel() {

    private val filtersRepository by lazy { FiltersRepository() }

    fun getIndicators() {
        _indicatorsList.value = Result.Loading
        viewModelScope.launch {
            _indicatorsList.value = filtersRepository.getIndicators()
        }
    }


    private val _rlFiltersList:
            MutableLiveData<Result<RlFiltersResponse>> = MutableLiveData()
    val rlFiltersList: LiveData<Result<RlFiltersResponse>>
        get() = _rlFiltersList


    private val _rlFilteredStocksList:
            MutableLiveData<Event<Result<RlFilteredStocksResponse>>> = MutableLiveData()
    val rlFilteredStocksList: LiveData<Event<Result<RlFilteredStocksResponse>>>
        get() = _rlFilteredStocksList

    fun rlFilterDescending(filterName: String) {
        _rlFilteredStocksList.value = Event(Result.Loading)
        viewModelScope.launch(Dispatchers.IO) {
            _rlFilteredStocksList.postValue(Event(filtersRepository.rlFilterDescending(filterName)))
        }
    }


    fun rlFilterAscending(filterName: String) {
        _rlFilteredStocksList.value = Event(Result.Loading)
        viewModelScope.launch(Dispatchers.IO) {
            _rlFilteredStocksList.postValue(Event(filtersRepository.rlFilterAscending(filterName)))
        }
    }

    fun getRlFilters() {
        _rlFiltersList.value = Result.Loading
        viewModelScope.launch(Dispatchers.IO) {
            _rlFiltersList.postValue(filtersRepository.getRlFilters())
        }
    }


    fun getSavedFilters() {
        viewModelScope.launch(Dispatchers.IO) {
            _savedFiltersList.postValue(filtersRepository.getSavedFilters())
        }
    }


    fun saveFilters(filterToBeSaved: FilterToBeSaved) {
        viewModelScope.launch(Dispatchers.IO) {
            filtersRepository.insertAllFilters(filterToBeSaved)
        }
    }

    fun searchForStocks() {
        _stocksList.value = Event(Result.Loading)
        viewModelScope.launch {
            _stocksList.value = Event(filtersRepository.searchForStocks(filtersList.value!!))
        }
        Log.d(TAG, "search for stocks")
    }


    private val TAG = this.javaClass.simpleName



    private val _comparator = MutableLiveData<String>("equal")
    val comparator: LiveData<String> = _comparator


    private val _filtersList = MutableLiveData<List<FilterItemsModel>>(listOf())
    val filtersList: LiveData<List<FilterItemsModel>> = _filtersList

    fun setComparator(comparator: String) {
        _comparator.value = comparator
    }


    fun resetComparator() {
        _comparator.value = null
    }

    private val _selectedIndicator = MutableLiveData<Pair<String, Int>>()
    val selectedIndicator: LiveData<Pair<String, Int>> = _selectedIndicator

    fun setIndicator(operand: Pair<String, Int>) {
        _selectedIndicator.value = operand
    }

    fun resetIndicator() {
        _selectedIndicator.value = null
    }

    private val _secondOperand = MutableLiveData<String>("5")
    val secondOperand: LiveData<String> = _secondOperand

    fun setSecondOperand(operand: String) {
        _secondOperand.value = operand
    }


    private val _indicatorsList:
            MutableLiveData<Result<IndicatorsResponse>> = MutableLiveData()
    val indicatorsList: LiveData<Result<IndicatorsResponse>>
        get() = _indicatorsList

    private val _stocksList:
            MutableLiveData<Event<Result<FilteredStocksResponse>>> = MutableLiveData()
    val stocksList: LiveData<Event<Result<FilteredStocksResponse>>>
        get() = _stocksList


    private val _savedFiltersList:
            MutableLiveData<List<FiltersWithFilterGroupWithParams>> = MutableLiveData()
    val savedFiltersList: LiveData<List<FiltersWithFilterGroupWithParams>>
        get() = _savedFiltersList

    fun setIndicatorsList(indicatorsList: Result<IndicatorsResponse>) {
        _indicatorsList.value = indicatorsList
    }


    fun setFiltersList(filters: List<FilterItemsModel>) {
        _filtersList.value = filters
    }

}