package com.example.tradingassistant.features.filter.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.tradingassistant.R
import com.example.tradingassistant.databinding.FragmentSavedFiltersBinding
import com.example.tradingassistant.features.filter.data.entity.FiltersWithFilterGroupWithParams

class SavedFiltersFragment : Fragment(), SavedFiltersListRecyclerViewAdapter.SavedFiltersCallback {

    private lateinit var binding: FragmentSavedFiltersBinding
    private val sharedViewModel: FiltersViewModel by activityViewModels()
    private lateinit var recyclerViewAdapter: SavedFiltersListRecyclerViewAdapter
    private val TAG = this.javaClass.simpleName


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_saved_filters, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        binding.apply {
            rvSavedFiltersList.setHasFixedSize(true)
            rvSavedFiltersList.layoutManager =
                LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
            recyclerViewAdapter = SavedFiltersListRecyclerViewAdapter(this@SavedFiltersFragment)
            rvSavedFiltersList.adapter = recyclerViewAdapter
        }


        sharedViewModel.savedFiltersList.observe(viewLifecycleOwner, {
            if (it.isEmpty())
                binding.tvEmptySavedFiltersList.visibility = View.VISIBLE
            else
                binding.tvEmptySavedFiltersList.visibility = View.INVISIBLE
            recyclerViewAdapter.submitList(it)
        })
    }

    override fun onResume() {
        super.onResume()
        sharedViewModel.getSavedFilters()
    }

    override fun onClick(item: FiltersWithFilterGroupWithParams) {
        TODO("Not yet implemented")
    }
}
