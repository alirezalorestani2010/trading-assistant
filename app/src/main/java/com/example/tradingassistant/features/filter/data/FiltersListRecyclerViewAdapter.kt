package com.example.tradingassistant.features.filter.data

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.tradingassistant.R
import com.example.tradingassistant.databinding.ItemFiltersListBinding

class FiltersListRecyclerViewAdapter :
    ListAdapter<FilterItemsModel, FiltersListRecyclerViewAdapter.RecyclerViewHolder>(object :
        DiffUtil.ItemCallback<FilterItemsModel>() {
        override fun areItemsTheSame(
            oldItem: FilterItemsModel,
            newItem: FilterItemsModel
        ): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(
            oldItem: FilterItemsModel,
            newItem: FilterItemsModel
        ): Boolean {
            return true

        }
    }) {

    private val TAG = "AddFilterBottomSheetDialogFragment"


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerViewHolder {
        val mBinding: ItemFiltersListBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.item_filters_list,
            parent,
            false
        )
        return RecyclerViewHolder(mBinding)
    }

    override fun onBindViewHolder(holder: RecyclerViewHolder, position: Int) {
        val item = getItem(position)
        holder.uBinding.filter = item
        holder.itemView.tag = item
        val params = StringBuilder()
        item.indicatorParams.forEach {
            params.append("${it.name}: ${it.value}\n")
        }
        Log.d(TAG, params.toString())
        val view = holder.uBinding
        view.tvParamsItemFiltersList.text = params.toString()
        view.tvParamsItemFiltersList
        view.clFiltersItem.setOnClickListener {
            val expanded = item.expanded
            item.expanded = !expanded
            notifyItemChanged(position)
        }
    }


    inner class RecyclerViewHolder(val uBinding: ItemFiltersListBinding) :
        RecyclerView.ViewHolder(uBinding.root)
}