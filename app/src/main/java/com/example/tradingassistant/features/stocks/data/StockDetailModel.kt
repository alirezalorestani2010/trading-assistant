package com.example.tradingassistant.features.stocks.data

import com.google.gson.annotations.SerializedName

data class StockDetailModel(
    @SerializedName("id") var id: String,
    @SerializedName("name") var name: String,
    @SerializedName("group") var group: String,
    @SerializedName("close") var close: Int,
    @SerializedName("trend") var trend: Int,
    @SerializedName("group_id") var groupId: Int,
    @SerializedName("RL") var RL: RlDetailModel
)