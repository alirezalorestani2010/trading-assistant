package com.example.tradingassistant.features.filter.data

data class IndicatorParam(
    val name: String,
    val value: Float
)
