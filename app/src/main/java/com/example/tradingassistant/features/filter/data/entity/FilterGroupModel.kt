package com.example.tradingassistant.features.filter.data.entity

data class FilterGroupModel(
    val name: String,
    val date: Long,
    val filters: List<FilterModel>
) {
    fun toFilterGroupEntity(): FilterGroupEntity {
        return FilterGroupEntity(name = name, insertDate = System.currentTimeMillis())
    }

}
