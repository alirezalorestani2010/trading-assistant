package com.example.tradingassistant.features.filter.data

import com.google.gson.annotations.SerializedName

data class RlFilterModel(
    @SerializedName("en_name") val enName: String,
    @SerializedName("fa_name") val faName: String
)
