package com.example.tradingassistant.features.stocks.data

data class VpModel(
    val x: Float,
    val y: Float
)
