package com.example.tradingassistant.features.filter.data

import com.example.tradingassistant.core.data.Result
import com.example.tradingassistant.features.filter.data.entity.FilterToBeSaved
import com.example.tradingassistant.features.filter.data.entity.FiltersWithFilterGroupWithParams
import com.example.tradingassistant.features.filter.data.entity.IndicatorsResponse

class FiltersRepository {


    private val filterRemoteDataSource: FilterRemoteDataSource by lazy {
        FilterRemoteDataSource()
    }

    private val filterLocalDataSource: FilterLocalDataSource by lazy {
        FilterLocalDataSource()
    }


    suspend fun getSavedFilters(): List<FiltersWithFilterGroupWithParams> =
        filterLocalDataSource.getSavedFilters()


    suspend fun insertAllFilters(filterToBeSaved: FilterToBeSaved) {
        filterLocalDataSource.insertAllFilters(filterToBeSaved)
    }

    suspend fun getIndicators(): Result<IndicatorsResponse>? =
        filterRemoteDataSource.getIndicators()

    suspend fun searchForStocks(indicatorsList: List<FilterItemsModel>): Result<FilteredStocksResponse> =
        filterRemoteDataSource.searchForStocks(indicatorsList)


    suspend fun getRlFilters(): Result<RlFiltersResponse> = filterRemoteDataSource.getRlFilters()

    suspend fun rlFilterDescending(filterName: String): Result<RlFilteredStocksResponse> =
        filterRemoteDataSource.rlFilterDescending(filterName)

    suspend fun rlFilterAscending(filterName: String): Result<RlFilteredStocksResponse> =
        filterRemoteDataSource.rlFilterAscending(filterName)
}