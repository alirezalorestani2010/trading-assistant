package com.example.tradingassistant.features.news.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.tradingassistant.R
import com.example.tradingassistant.databinding.ItemNewsListBinding
import com.example.tradingassistant.features.news.data.NewsModel

class NewsListRecyclerViewAdapter(
    private val callback: CallBack
) : ListAdapter<NewsModel, NewsListRecyclerViewAdapter.RecyclerViewHolder>(object :
    DiffUtil.ItemCallback<NewsModel>() {
    override fun areItemsTheSame(oldItem: NewsModel, newItem: NewsModel): Boolean {
        return oldItem.desc == newItem.desc
    }

    override fun areContentsTheSame(oldItem: NewsModel, newItem: NewsModel): Boolean {
        return true

    }
}) {

    interface CallBack {
        fun onItemClick(item: NewsModel)
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerViewHolder {
        val mBinding: ItemNewsListBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.item_news_list,
            parent,
            false
        )
        return RecyclerViewHolder(mBinding)
    }

    override fun onBindViewHolder(holder: RecyclerViewHolder, position: Int) {
        val item = getItem(position)
        holder.uBinding.news = item
        holder.itemView.tag = item
        val view = holder.uBinding

        view.clNewsListItem.setOnClickListener {
            callback.onItemClick(item)
        }

    }


    inner class RecyclerViewHolder(val uBinding: ItemNewsListBinding) :
        RecyclerView.ViewHolder(uBinding.root)
}