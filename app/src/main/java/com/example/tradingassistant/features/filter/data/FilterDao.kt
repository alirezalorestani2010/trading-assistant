package com.example.tradingassistant.features.filter.data

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Transaction
import com.example.tradingassistant.features.filter.data.entity.*

@Dao
interface FilterDao {


    @Transaction
    suspend fun getAllSavedFilters(): List<FiltersWithFilterGroupWithParams> {
        val result = arrayListOf<FiltersWithFilterGroupWithParams>()
        val allGroups = getAllFilterGroups()
        val allFilters = getAllFilters()
        val allParams = getAllFilterParams()
        for (group in allGroups) {
            val filterWithParam = arrayListOf<FiltersWithParam>()
            val filters = allFilters.filter { it.filterGroupId == group.groupId }
            for (filter in filters) {
                val params = allParams.filter { it.filterId == filter.filterId }
                filterWithParam.add(FiltersWithParam(filter, params))
            }
            result.add(FiltersWithFilterGroupWithParams(group, filterWithParam))
        }
        return result
    }

    @Query("SELECT * FROM FILTER_GROUP_TABLE")
    suspend fun getAllFilterGroups(): List<FilterGroupEntity>


    @Query("SELECT * FROM FILTER_PARAM_TABLE")
    suspend fun getAllFilterParams(): List<FilterParamEntity>


    @Query("SELECT * FROM SAVED_FILTERS_TABLE")
    suspend fun getAllFilters(): List<FilterEntity>

    @Insert
    suspend fun insertFilter(filter: FilterEntity): Long


    @Insert
    suspend fun insertFilters(filters: List<FilterEntity>)

    @Insert
    suspend fun insertFiltersGroup(filtersGroup: FilterGroupEntity): Long

    @Insert
    suspend fun insertFiltersParams(filterParams: FilterParamEntity)

    @Transaction
    suspend fun insertAllFilters(filterToBeSaved: FilterToBeSaved) {
        val groupId = insertFiltersGroup(filterToBeSaved.group.toFilterGroupEntity())
        for (filter in filterToBeSaved.group.filters) {
            val filterId = insertFilter(filter = filter.toFilterEntity(groupId))
            for (filterParam in filter.params) {
                insertFiltersParams(filterParam.toFilterParamEntity(filterId))
            }
        }
    }
}
