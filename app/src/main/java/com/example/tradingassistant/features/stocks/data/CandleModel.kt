package com.example.tradingassistant.features.stocks.data

data class CandleModel(
    val High: Float,
    val Low: Float,
    val Open: Float,
    val Close: Float,
    val Index: Int,
)
