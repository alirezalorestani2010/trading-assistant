package com.example.tradingassistant.features.filter.data

import com.example.tradingassistant.core.data.Result
import com.example.tradingassistant.core.data.RetrofitFactory
import com.example.tradingassistant.features.filter.data.entity.IndicatorsResponse
import retrofit2.Retrofit

class FilterRemoteDataSource {


    private val retrofit: Retrofit by lazy { RetrofitFactory.retrofit }
    private val service: FilterService by lazy {
        retrofit.create(FilterService::class.java)
    }


    suspend fun getIndicators()
            : Result<IndicatorsResponse>? {
        return try {
            val response = service.getIndicators()
            if (response.isSuccessful) {
                response.body()?.let {
                    Result.Success(it)
                }
            } else
                Result.Error(response.message())

        } catch (e: Exception) {
            Result.Error(e.message ?: "EX")
        }
    }

    suspend fun searchForStocks(indicatorsList: List<FilterItemsModel>): Result<FilteredStocksResponse> {
        try {
            val response = service.searchForStocks(
                FiltersToBeSearchedModel(indicatorsList)
            )
            if (response.isSuccessful) {
                response.body()?.let {
                    return Result.Success(it)
                }
            } else
                return Result.Error(response.message())

        } catch (e: Exception) {
            return Result.Error(e.message ?: "EX")
        }
        return Result.Error("EX")

    }

    suspend fun getRlFilters(): Result<RlFiltersResponse> {
        try {
            val response = service.getRlFilters()
            if (response.isSuccessful) {
                response.body()?.let {
                    return Result.Success(it)
                }
            } else
                return Result.Error(response.message())
        } catch (e: Exception) {
            return Result.Error(e.message ?: "EX")
        }
        return Result.Error("EX")
    }

    suspend fun rlFilterDescending(filterName: String): Result<RlFilteredStocksResponse> {
        try {
            val response = service.rlFilterDescending(filterName)
            if (response.isSuccessful) {
                response.body()?.let {
                    return Result.Success(it)
                }
            } else
                return Result.Error(response.message())
        } catch (e: Exception) {
            return Result.Error(e.message ?: "EX")
        }
        return Result.Error("EX")
    }

    suspend fun rlFilterAscending(filterName: String): Result<RlFilteredStocksResponse> {
        try {
            val response = service.rlFilterAscending(filterName)
            if (response.isSuccessful) {
                response.body()?.let {
                    return Result.Success(it)
                }
            } else
                return Result.Error(response.message())
        } catch (e: Exception) {
            return Result.Error(e.message ?: "EX")
        }
        return Result.Error("EX")
    }
}